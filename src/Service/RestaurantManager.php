<?php

namespace App\Service;

use App\Entity\Restaurant;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RestaurantManager
{
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getList(): array
    {
        $response = $this->httpClient->request(
            "GET",
            "https://private-anon-a3c5e29d18-idcrestaurant.apiary-mock.com/restaurant",
            [
                "headers" => [
                    "Content-Type" => "application/json",
                ],
            ]
        );

        return $response->toArray();
    }

}