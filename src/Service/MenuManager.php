<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class MenuManager
{
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getMenuByRestaurantId($restaurantId): array
    {
        $response = $this->httpClient->request(
            "GET",
            "https://private-anon-a3c5e29d18-idcrestaurant.apiary-mock.com/daily-menu?restaurant_id=$restaurantId",
            [
                "headers" => [
                    "Content-Type" => "application/json",
                ],
            ]
        );
        return $response->toArray();
    }

}