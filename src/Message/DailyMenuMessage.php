<?php

namespace App\Message;

final class DailyMenuMessage
{
    /*
     * Add whatever properties & methods you need to hold the
     * data for this message class.
     */

     private $email;
     private $menus;

     public function __construct(string $email, array $menus)
     {
         $this->email = $email;
         $this->menus = $menus;
     }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getMenus(): array
    {
        return $this->menus;
    }

}
