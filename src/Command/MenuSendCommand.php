<?php

namespace App\Command;

use App\Message\DailyMenuMessage;
use App\Repository\RestaurantSubscriptionRepository;
use App\Service\MenuManager;
use App\Service\RestaurantManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

class MenuSendCommand extends Command
{
    protected static $defaultName = 'app:menu:send';

    private $bus;
    private $menuManager;
    private $subscriptionRepository;

    public function __construct(MessageBusInterface $bus, MenuManager $menuManager, RestaurantSubscriptionRepository $subscriptionRepository)
    {
        $this->bus = $bus;
        $this->menuManager = $menuManager;
        $this->subscriptionRepository = $subscriptionRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Sends menus to subscribed emails.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $menus = [];
        $menusGroupedByEmails = [];
        $subscribes = $this->subscriptionRepository->findAll();
        foreach ($subscribes as $subscribe) {
            $email = $subscribe->getEmail();
            $restaurantId = $subscribe->getRestaurantId();

            if (!isset($menus[$restaurantId])) {
                $menus[$restaurantId] = $this->menuManager->getMenuByRestaurantId($subscribe->getRestaurantId());
            }

            $menusGroupedByEmails[$email][] = $menus[$restaurantId];
        }

        foreach ($menusGroupedByEmails as $email => $menus) {
            $this->bus->dispatch(new DailyMenuMessage($email, $menus));
        }

        return Command::SUCCESS;
    }
}
