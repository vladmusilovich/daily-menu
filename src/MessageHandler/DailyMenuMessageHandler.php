<?php

namespace App\MessageHandler;

use App\Message\DailyMenuMessage;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Address;

final class DailyMenuMessageHandler implements MessageHandlerInterface
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(DailyMenuMessage $message)
    {
        $email = (new TemplatedEmail())
            ->from('dailymenu@akdev.by')
            ->to(new Address($message->getEmail()))
            ->htmlTemplate('emails/menus.html.twig')
            ->context([
                'restaurantMenus' => $message->getMenus(),
            ]);

        $this->mailer->send($email);
    }
}
