<?php

namespace App\Controller;

use App\Service\MenuManager;
use App\Service\RestaurantManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class RestaurantController extends AbstractController
{
    #[Route('/', name: 'restaurant_index')]
    public function index(RestaurantManager $restaurantManager, SerializerInterface $serializer): Response
    {
        $restaurants = $restaurantManager->getList();
        return $this->render('restaurant/index.html.twig', [
            'controller_name' => 'RestaurantController',
            'restaurants' => $restaurants,
        ]);
    }

    #[Route('restaurants/{id}/menu', name: 'restaurant_menu')]
    public function showMenu(MenuManager $menuManager, $id)
    {
        $menus = $menuManager->getMenuByRestaurantId($id);
        return $this->render('restaurant/menu.html.twig', [
            'controller_name' => 'RestaurantController',
            'menus' => $menus,
        ]);
    }
}
