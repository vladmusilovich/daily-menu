<?php

namespace App\Controller;

use App\Entity\RestaurantSubscription;
use App\Repository\RestaurantSubscriptionRepository;
use App\Service\RestaurantManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RestaurantSubscriptionController extends AbstractController
{
    #[Route('/restaurant/subscription', name: 'restaurant_subscription')]
    public function new(Request $request, RestaurantManager $restaurantManager, RestaurantSubscriptionRepository $repository)
    {
        $choices = [];
        $restaurants = $restaurantManager->getList();
        foreach ($restaurants as $restaurant) {
            $choices[$restaurant['name']] = $restaurant['id'];
        }

        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, ['label' => 'Email'])
            ->add('restaurants', ChoiceType::class, [
                'label' => 'Restaurants',
                'required' => true,
                'multiple' => true,
                'choices' => $choices,
            ])
            ->add('save', SubmitType::class, ['label' => 'Subscribe'])
            ->setAction('/restaurant/subscription')
            ->setMethod('POST')
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $formData = $form->getData();
            foreach ($formData['restaurants'] as $restaurantId) {
                $restaurantSubscription = $repository->findBy([
                    'email' => $formData['email'],
                    'restaurant_id' => $restaurantId,
                ]);
                if (!$restaurantSubscription) {
                    $newRestaurantSubscription = new RestaurantSubscription();
                    $newRestaurantSubscription->setEmail($formData['email']);
                    $newRestaurantSubscription->setRestaurantId($restaurantId);
                    $entityManager->persist($newRestaurantSubscription);
                    $entityManager->flush();
                }
            }

            return $this->redirectToRoute('restaurant_index');
        }

        return $this->render(
            'restaurant_subscription/_subscription_form.html.twig', [
                'form' => $form->createView(),
        ]);
    }
}
