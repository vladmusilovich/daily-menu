# Daily Menu Test Application

## Before Start
Install [Docker](https://docs.docker.com/get-docker/)

## Clone the repository
````
git clone https://vladmusilovich@bitbucket.org/vladmusilovich/daily-menu.git
````

## Run project
````
sudo docker-compose up -d --build
````

## Install Project Dependencies
````
cp .env.example .env
sudo docker exec -ti daily-meny-php-fpm bash
composer install
````

## Finish

Open page http://localhost:8080/

## Command to send a menus to an emails

1. Specify your `MAILER_DSN` in your `.env` file
2. Open `daily-menu-php-fpm` container: 
````
sudo docker exec -ti daily-meny-php-fpm bash
````
3. Run `symfony console app:menu:send`